# README #

### What is this repository for? ###

Web Page to display reports on Dashboards

### How do I get set up? ###

Add report you want to be displayed in the "Reports" Folder.
HTML looks for "Report1.png" to display.

### Contribution guidelines ###

Get in contact with owner. 

### Who do I talk to? ###

Andre Louw
alouw@apcglobal.com